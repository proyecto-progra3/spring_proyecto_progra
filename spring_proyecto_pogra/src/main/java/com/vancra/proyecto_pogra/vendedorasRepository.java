package com.vancra.proyecto_pogra;
import org.springframework.data.jpa.repository.JpaRepository;
import java.io.Serializable;
import java.util.List;

public interface vendedorasRepository extends JpaRepository<vendedoras,Serializable>{
	List <vendedoras> findByNombre(String nombre);
	
}
